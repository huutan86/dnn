"""Provides functionality to build a DNN.

** Copyright Tan H. Nguyen - 2020**
"""
from typing import Dict
from typing import List
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
from absl import logging

from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.initializer import InitializationMethod
from dnn.core.layers import BaseLayer
from dnn.core.layers import Convolutional
from dnn.core.layers.dropout import DropoutLayer
from dnn.core.loss import compute_total_loss
from dnn.core.loss import LossType
from dnn.util import accuracy
from dnn.util import random_mini_batches


class DNN:
    """A class for training and testing a deep neural network.

    Args:

    Attributes:
        layers: Different layers of the DNN.
        costs: The cost values w.r.t the number of iterations.
        l2_lambd: The factor used in the L2-regularization. Defaults to float, in which the L2-regularization will not
            be performed.
        batch_normalization (optional): If True, perform the batch normalization.
        initialization_method (optional): The type of initialization method. Defaults to InitializationMethod.He
    """

    def __init__(
        self,
        l2_lambd: float = 0,
        batch_normalization: bool = False,
        initialization_method: InitializationMethod = InitializationMethod.He,
        loss_type: LossType = LossType.LogisticRegressionCrossEntropy,
    ):
        """Initializes a DNN class object."""
        self.layers: List[BaseLayer] = []
        self.costs: List[float] = []
        self._l2_lambd = l2_lambd
        self._batch_normalization = batch_normalization
        self._initialization_method = initialization_method
        self._loss_type = loss_type

    @property
    def loss_type(self) -> LossType:
        """Returns the type of loss used by the network."""
        return self._loss_type

    @property
    def l2_lambda(self) -> float:
        """Returns the weight for the L2 regularization"""
        return self._l2_lambd

    def add_layer(self, layer: BaseLayer):
        """Adds a layer to the DNN."""

        # Make sure that the l2 is consistent
        if isinstance(layer, FullyConnectedLayer):
            layer.l2_lambd = self._l2_lambd
        self.layers.append(layer)

    def print_network(self):
        """Prints different layers of the network."""
        for layer in self.layers:
            logging.info(layer.__repr__())

    def _initialize_parameters(self):
        """Initializes parameters of the networks."""
        logging.info("Initializing parameters of the network")
        num_layers = len(self.layers)  # Number of layers, including the input layer.
        np.random.seed(3)
        for layer_index in range(1, num_layers):
            if not isinstance(self.layers[layer_index], Convolutional):
                self.layers[layer_index].initialize_params(
                    num_neuron_prev_layer=self.layers[layer_index - 1].num_neurons
                )

    @property
    def parameters(self) -> List[Optional[Dict[str, np.ndarray]]]:
        """Returns a list of the parameters for different layers of the network."""
        return [layer.parameters for layer in self.layers]

    def propagate_forward(self, input_val: np.ndarray) -> np.ndarray:
        """Propagates in the forward direction.

        Args:
             input_val: The input data that we want to propagate through different layers. It has a size of (n_x, m)
                where m is the number of samples.
        Returns:
            Activation output of the last layer.
        """
        a_val = input_val
        for layer_index in range(len(self.layers)):
            a_val = self.layers[layer_index].propagate_forward(a_val)
            # Cache the activation output to the next layer.
            if layer_index < len(self.layers) - 1:
                self.layers[layer_index + 1].a_prev = a_val

        return a_val

    def propagate_forward_without_updating_layer_cache(self, input_val: np.ndarray):
        """Propagates in the forward direction, bypasses all the drop out.

        This function is normally used in the testing.
        Args:
             input_val: The input data that we want to propagate through different layers. It has a size of (n_x, m)
                where m is the number of samples.
        Returns:
            Activation output of the last layer.
        """
        a_val = input_val
        for layer_index in range(len(self.layers)):
            if not isinstance(self.layers[layer_index], DropoutLayer):
                a_val = self.layers[layer_index].propagate_forward_without_updating_cache(a_val)
        return a_val

    def propagate_backward(self, yhat_val: np.ndarray, y_val: np.ndarray):
        """Propagates in the backward direction, calculates the derivative of the cost function to W^[l] and b^[l].

        Args:
            yhat_val: activation output of the last layer, which is an array of size (n^[l], m).
            y_val: label of the data.
        """
        # Initialize da^[L] of the last layer, which is a matrix of size (n^[L], m)
        if self._loss_type == LossType.LogisticRegressionCrossEntropy:
            da_val = -y_val / yhat_val + (1 - y_val) / (1 - yhat_val)
        if self._loss_type == LossType.SoftmaxCrossEntropy:
            da_val = -y_val / yhat_val

        # Go through different layers and update the network.
        for layer in reversed(self.layers):
            # Only update if the current layer is not the input layer.
            if not isinstance(layer, InputLayer):
                da_val = layer.propagate_backward(da_val)

    def update_parameters(self, learning_rate: float = 0.1):
        """Updates all parameters of the network.

        Args:
            learning_rate: A number that determines how fast we should update the parameters.
        """
        for layer in self.layers:
            if not isinstance(layer, InputLayer):
                layer.update_params(learning_rate=learning_rate)

    def train(
        self,
        training_x: np.ndarray,
        training_y: np.ndarray,
        dev_x: Optional[np.ndarray] = None,
        dev_y: Optional[np.ndarray] = None,
        learning_rate: float = 0.0075,
        num_epochs: int = 3000,
        mini_batch_size: Optional[int] = None,
        print_cost: bool = False,
        num_epoch_log: int = 100,
        **kwargs,
    ):
        """Trains the neural network.

        Args:
            training_x: A matrix of size (n_x, m) where n_x is the number of features, m is the number of training
                samples.
            training_y: A matrix of size (1, m) for the labels of training_x.
            dev_x: A matrix of size (n_x, m_dev) where m_dev is the number of dev samples. Defaults to None
            dev_y: A matrix for size (1, m_dev) the labels of dev_v. Defaults to None.
            learning_rate (optional): The updating rate of the parameter.
            num_epochs (optional): The number of training epochs (passes through the sample).
            mini_batch_size (optional): The size of the minibatch that we want to use. Defaults to None, in which case,
                batch gradient descent will be used.
            print_cost (optional): If true, the training cost will be printed.
            num_epoch_log (optional): After a multiple of the number of epochs, do the logging, defaults to 100.
            kwargs: All remaining parameters of the network.
        """
        self._initialize_parameters()
        seed = 10
        m = training_x.shape[1]
        for epoch in range(num_epochs):
            seed += 1
            cost = 0
            if mini_batch_size is None:
                mini_batch_size = training_x.shape[1]

            mini_batches = random_mini_batches(training_x, training_y, mini_batch_size=mini_batch_size, seed=seed)

            for mini_batch in mini_batches:
                mini_batch_x, mini_batch_y = mini_batch

                # Propagate forward
                mini_batch_y_hat = self.propagate_forward(mini_batch_x)

                # Compute the cost
                cost += sum(
                    compute_total_loss(
                        y_hat=mini_batch_y_hat,
                        y=mini_batch_y,
                        loss_type=self._loss_type,
                        l2_lambd=self._l2_lambd,
                        network_params=self.parameters,
                    )
                )

                # Propagate backward
                self.propagate_backward(yhat_val=mini_batch_y_hat, y_val=mini_batch_y)

                # Update parameters
                self.update_parameters(learning_rate)

            cost_avg = cost / m

            if print_cost and epoch % num_epoch_log == 0:
                # Compute the predicted label during the training
                dev_accuracy_str = (
                    f"accuracy on dev set = {accuracy(self.predict(dev_x), dev_y):1.3f}, " if dev_x is not None else ""
                )

                logging.info(f"Epoch {epoch}, avg. cost = {cost_avg:1.6f}, " + dev_accuracy_str)
            self.costs.append(cost)
        self._plot_cost_function(self.costs)

    @staticmethod
    def _plot_cost_function(costs: List[float]) -> None:
        """Plots the cost function vs. epochs"""
        plt.figure()
        plt.plot(np.array(range(len(costs))) / 100, costs)
        plt.xlabel("Epochs (per 100)")
        plt.ylabel("Cost")
        plt.pause(0.01)

    def predict(self, testing_x: np.ndarray) -> np.ndarray:
        """Predicts the label of the testing data.

        During the the predict, we don't want to turn on any drop out.
        Args:
            testing_x: An array of size (n_x, m_test) that contains the testing data.
        Returns:
            A numpy array that contains the predicted label.
        """
        a_val = self.propagate_forward_without_updating_layer_cache(testing_x)
        temp = np.zeros((1, testing_x.shape[1]))
        temp[a_val > 0.5] = 1
        return temp
