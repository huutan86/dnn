"""Provides functionalities to update the parameters."""
from abc import ABC
from abc import abstractmethod
from enum import auto
from enum import Enum
from typing import Dict
from typing import Optional

import numpy as np


class OptimizerType(Enum):
    GradientDescent = auto()
    Momentum = auto()
    Adam = auto()


class AbstractOptimizer(ABC):
    """Abstract base class for optimization."""

    def __init__(self) -> None:
        self._update = None

    @property
    def update(self) -> Optional[Dict[str, np.ndarray]]:
        """Returns the quantity to update the parameters."""
        return self._update

    @abstractmethod
    def compute_update(self, grads: Dict[str, np.ndarray]) -> None:
        """Computes the quantity to update from the gradients."""
        raise NotImplementedError


class GradientDescentOptimizer(AbstractOptimizer):
    """The optimizer with gradient descent."""

    def compute_update(self, grads: Dict[str, np.ndarray]) -> None:
        self._update = grads


class MomentumOptimizer(AbstractOptimizer):
    """The momentum optimizer.

    Args:
        beta (optional): The constant beta to compute the `update`. Defaults to 0.9.
    """

    def __init__(self, beta=0.9) -> None:
        super().__init__()
        self._beta = beta

    def compute_update(self, grads: Dict[str, np.ndarray]) -> None:
        if self._update is None:
            self._update = {k: np.zeros_like(v) for k, v in grads.items()}
        self._update = {k: self._beta * self._update[k] + (1 - self._beta) * grads[k] for k in grads}


class AdamOptimizer(AbstractOptimizer):
    """The Adam optimizer.

    Reference:
            https://arxiv.org/pdf/1412.6980.pdf
    Args:
        beta1 (optional): The constant to compute v. Defaults to 0.9.
        beta2 (optional): The constant to compute s. Defaults to 0.999.
        eps (optional): The constant used to avoid zero division. Defaults to 1e-8.
    Attributes:
        v: The variable v used in Adam.
        s: The variable s used in Adam.
        t: The step count used in Adam.
    """

    def __init__(self, beta1=0.9, beta2=0.999, eps=1e-8) -> None:
        super().__init__()
        self._beta1 = beta1
        self._beta2 = beta2
        self._eps = eps
        self.v: Dict[str, np.ndarray] = None
        self.s: Dict[str, np.ndarray] = None
        self.t = 1

    def compute_update(self, grads: Dict[str, np.ndarray]) -> None:
        if self.v is None:
            self.v = {k: np.zeros_like(v) for k, v in grads.items()}
        if self.s is None:
            self.s = {k: np.zeros_like(v) for k, v in grads.items()}

        self.v = {k: self._beta1 * self.v[k] + (1 - self._beta1) * grads[k] for k in self.v}
        v_corrected = {k: self.v[k] / (1 - self._beta1 ** self.t) for k in self.v}

        self.s = {k: self._beta2 * self.s[k] + (1 - self._beta2) * np.square(grads[k]) for k in self.v}
        s_corrected = {k: self.s[k] / (1 - self._beta2 ** self.t) for k in self.s}

        self._update = {k: v_corrected[k] / (np.sqrt(s_corrected[k] + self._eps)) for k in v_corrected}
        self.t += 1


def optimizer(optimizer_type: OptimizerType, **kwargs) -> AbstractOptimizer:
    """Returns an optimizer."""
    if optimizer_type == OptimizerType.GradientDescent:
        return GradientDescentOptimizer()
    if optimizer_type == OptimizerType.Momentum:
        return MomentumOptimizer(**kwargs)
    if optimizer_type == OptimizerType.Adam:
        return AdamOptimizer(**kwargs)
