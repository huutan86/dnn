"""Provides functionality to compute the cost function.

** Tan H. Nguyen **
"""
from enum import auto
from enum import Enum
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple

import numpy as np


class LossType(Enum):
    LogisticRegressionCrossEntropy = auto()  # Used for binary classification.
    SoftmaxCrossEntropy = auto()


def compute_total_loss(
    y_hat: np.ndarray,
    y: np.ndarray,
    network_params: List[Optional[Dict[str, np.ndarray]]],
    l2_lambd: float,
    loss_type: LossType,
) -> Tuple[float, float]:
    """Returns a tuple of regularization cost (not normalized to the number of samples) and cost without regularization.

    Args:
        y_hat: A (1, m) array of the activation output of the network where m is the number of samples.
        y: A (1, m) array of 0 and 1 correspond to the labels of the samples.
        network_params: The parameters of the network layers. Defaults to an empty list
        loss_type: The type of the lost. Default to LossType.LogisticRegressionCrossEntropy for binary classification.
        l2_lambd: The factor used for computing the L2 regularization loss.
    """
    l2_regularization_loss = _l2_regularization_loss(l2_lambd=l2_lambd, network_params=network_params)
    loss_without_regularization = compute_no_regularization_loss(y_hat, y, loss_type=loss_type)
    return l2_regularization_loss, loss_without_regularization


def _l2_regularization_loss(l2_lambd: float = 0, network_params: List[Optional[Dict[str, np.ndarray]]] = []) -> float:
    """Computes the loss from the L2-regularization term."""
    w_norms_squared = map(
        lambda x: np.square(np.linalg.norm(x)), [param["W"] for param in network_params if param != {}]
    )
    return l2_lambd * sum(w_norms_squared) / 2


def compute_no_regularization_loss(y_hat: np.ndarray, y: np.ndarray, loss_type: LossType) -> float:
    """Compute the cost-function between the prediction y_hat and the true label y.

    Args:
        y_hat: A (1, m) array of the activation output of the network where m is the number of samples.
        y: A (1, m) array of 0 and 1 correspond to the labels of the samples.
        loss_type: The type of the lost.
    """
    assert y_hat.shape == y.shape
    if not all(label.all() in [0, 1] for label in y.ravel()):
        raise ValueError("Logistic Regression Cross entropy loss is used, 'y' must only contain 0 and 1.")

    if loss_type == LossType.LogisticRegressionCrossEntropy:
        return -np.sum(y * np.log(y_hat) + (1 - y) * np.log(1 - y_hat))

    if loss_type == LossType.SoftmaxCrossEntropy:
        if y_hat.shape[0] < 2:
            raise ValueError("Softmax Cross entropy is used, number of row must be at least 2.")
        # Check for one hot encoding.
        if not all(col.all() == 1 for col in np.sum(y_hat, axis=0)):
            raise ValueError("Softmax Cross entropy requires one-hot encoding.")
        return -np.sum(y * np.log(y_hat))
