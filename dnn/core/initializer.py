"""Provides functionality to initialize the parameter."""
from enum import auto
from enum import Enum
from typing import Dict

import numpy as np


class InitializationMethod(Enum):
    ConstScaling = auto()
    He = auto()
    Xavier = auto()


def initialize_params(
    num_neurons: int, num_neuron_prev_layer: int, init_method: InitializationMethod = InitializationMethod.He
) -> Dict[str, np.ndarray]:
    temp = {"W": None, "b": None}
    if init_method == InitializationMethod.ConstScaling:
        temp["W"] = _initialize_w_with_const_scaling(num_neurons, num_neuron_prev_layer)
    elif init_method == InitializationMethod.He:
        temp["W"] = _initialize_w_with_he_method(num_neurons, num_neuron_prev_layer)
    elif init_method == InitializationMethod.Xavier:
        temp["W"] = _initialize_w_with_xavier_method(num_neurons, num_neuron_prev_layer)
    temp["b"] = np.zeros((num_neurons, 1))
    return temp


def _initialize_w_with_const_scaling(num_neurons: int, num_neuron_prev: int, const_value: float = 0.01):
    """Initializes w with np.random.randn * const_value."""
    return np.random.randn(num_neurons, num_neuron_prev) * const_value


def _initialize_w_with_he_method(num_neurons: int, num_neuron_prev: int):
    """Initializes w with np.random.randn * np.sqrt(2 / num_neuron_prev).

    Reference:
        He et al, 2015: which can be found at https://arxiv.org/abs/1502.01852
        See a very nice derivation here: https://medium.com/@shoray.goel/kaiming-he-initialization-a8d9ed0b5899
    """
    return np.random.randn(num_neurons, num_neuron_prev) * np.sqrt(2 / num_neuron_prev)


def _initialize_w_with_xavier_method(num_neurons: int, num_neuron_prev: int):
    """Initializes w with np.random.randn * np.sqrt(1 / num_neuron_prev).

    Reference:
        https://prateekvishnu.medium.com/xavier-and-he-normal-he-et-al-initialization-8e3d7a087528
    """
    return np.random.randn(num_neurons, num_neuron_prev) * np.sqrt(1 / num_neuron_prev)
