from typing import Dict
from typing import Optional
from typing import Tuple

import numpy as np

from dnn.core.activation import AbstractActivationFunction
from dnn.core.initializer import InitializationMethod
from dnn.core.initializer import initialize_params
from dnn.core.layers.layer import BaseLayer
from dnn.core.optimization import AbstractOptimizer
from dnn.core.optimization import AdamOptimizer


class FullyConnectedLayer(BaseLayer):
    """The fully connected layer of the neural network

    Args:
        num_neurons: The number of neurons of the current layer.
        activation_func: The activation function.
        optimizer (optional): The type of the optimizer to use. This affects how we update the parameters. Defaults
            to Adam
        l2_lambd (optional): The weight used in L2-regularization. Defaults to 0.

    Attributes:
        grads: Gradients of the cost functions w.r.t W^[l] and b^[l], namely dW^[l] and db^[l].
        num_neurons: Number of neurons for a layer.
        a_prev: The activation output of the previous layer.
        dz_val: Gradients of the cost function w.r.t Z^[l].
    """

    def __init__(
        self,
        num_neurons: int,
        activation_func: Optional[AbstractActivationFunction],
        optimizer: AbstractOptimizer = AdamOptimizer(),
        l2_lambd: float = 0.0,
    ):
        """Initializer for a fully connected layer."""
        super().__init__(num_neurons=num_neurons, activation_func=activation_func, optimizer=optimizer)
        self.grads: Dict[str : np.ndarray] = {}
        self.a_prev: Optional[np.ndarray] = None
        self.dz_val: Optional[np.ndarray] = None
        self.l2_lambd = l2_lambd

    def initialize_params(
        self, num_neuron_prev_layer: int, init_method: InitializationMethod = InitializationMethod.He
    ) -> None:
        """Initializes the parameters for the current layer."""
        self.params = initialize_params(self.num_neurons, num_neuron_prev_layer, init_method=init_method)

    @staticmethod
    def linear_forward(a_val: np.ndarray, w: np.ndarray, b: np.ndarray) -> np.ndarray:
        """Computes the hidden variables Z^[l] of the layer given its input (A^[l-1]).

        Z^[l] = W^[l] * input + b^[l].
        Z^[l] will have a dimension of (n^[l] x m) where n^[l] = number of neurons, m is the number of training samples
        (=input.shape[1])

        Args:
            a_val: the input to the current layer, which is the activation output from the previous layer. It has
                size of (n^[l-1], m)
            w: The weight matrix that has size of (n^[l], n^[l-1])
            b: The offset maxtrix of size (n^[l], 1)

        Returns:
            The value of the hidden variable
        """
        assert w.shape[1] == a_val.shape[0]
        assert b.shape == (w.shape[0], 1)
        return np.dot(w, a_val) + b

    def linear_backward(self, dz_val: np.ndarray, a_val_prev: np.ndarray) -> Tuple[np.ndarray, ...]:
        """Computes dA^[l-1], dW^[l], db^[l] from dZ^[l] after we back propagate behind the activation function.

        Note that Z^[l] = W^[l] * A^[l-1] + b^[l]. Hence,
            dA^[l-1] = W^[l]T x dZ^[l]. Dimension-wise, this will be (n^[l-1], n^[l]) x (n^[l], m).
            dW^[l] = (1/m) dZ^[l] x A^[l-1]^T. Dimension-wise, this will be (n^[l], n^[l-1]). There is a factor or 1/m
                because each dZ column is dependent on W and we have m terms of dZ^[l]. One for each sample.
            db^[l] = (1/m) sum over samples (dZ^[l])

        Args:
            dz_val: The array dZ^[l] of size (n^[l] x m) where n^[l] = num_neurons and m be the number of training
                samples
            a_val_prev: The array A^[l-1] of size (n^[l-1], m).

        Returns:
            (dA^[l-1], dW^[l], db^[l]) be the derivatives of the cost function w.r.t the activation output and the
                parameters of the current layer. dA^[l-1] has the shape of (n^[l-1], m)
        """
        m = dz_val.shape[1]
        da_prev = np.dot(self.params["W"].T, dz_val)
        dw = (1 / m) * np.dot(dz_val, a_val_prev.T) + self.l2_lambd / m * self.params["W"]
        db = (1 / m) * np.sum(dz_val, axis=1, keepdims=True)
        return da_prev, dw, db

    def _propagate_forward(
        self,
        a_prev: np.ndarray,
    ) -> Tuple[np.ndarray]:
        z_val = self.linear_forward(a_prev, w=self.params["W"], b=self.params["b"])
        a_val = self._activation_func.propagate_forward(z_val)
        return z_val, a_val

    def propagate_backward(self, da_val: np.ndarray) -> np.ndarray:
        """Computes dA^[l-1] from the input dA^[l] (da_val)

        Args:
            da_val: The derivative of the cost w.r.t A^[l]. This matrix has dimensions of (n^[l], m).
        Returns:
            The matrix that corresponds to dA^[l-1] of size (n^[l-1], m).
        """
        # Propagates through the activation.
        dz_val = self._activation_func.propagate_backward(da_val, self.z_val)
        da_val_prev, dw, db = self.linear_backward(
            dz_val,
            a_val_prev=self.a_prev,
        )
        self.grads = {"dw": dw, "db": db}
        self.dz_val = dz_val
        return da_val_prev

    def update_params(self, learning_rate: float) -> None:
        self.optimizer.compute_update(self.grads)
        self._update_params_with_quantity(quant=self.optimizer.update, learning_rate=learning_rate)

    def _update_params_with_quantity(self, quant: Dict[str, np.ndarray], learning_rate: float = 0.01):
        """Updates the parameters with a residue quantity."""
        self.params["W"] -= learning_rate * quant["dw"]
        self.params["b"] -= learning_rate * quant["db"]
