from abc import ABC
from enum import auto
from enum import Enum
from typing import Dict
from typing import Optional
from typing import Tuple

import numpy as np

from dnn.core.activation import AbstractActivationFunction
from dnn.core.optimization import AbstractOptimizer


class ForwardPropagationMode(Enum):
    """The forward propagation mode."""

    Train = auto()
    Predict = auto()


class BaseLayer(ABC):
    """A class that defines a layer of a neural network.

    Args:
        num_neurons: Number of neurons in this layers.
        activation_func (optional): Th activation function. Defaults to None.
        optimizer (optional): The optimizer that we used for optimization. Defaults to None

    Attributes:
        params: A dictionary that contains all the parameters of the network.
        num_neurons: Number of neurons for a layer.
        optimizer: The optimizer that we used for optimization
    """

    def __init__(
        self,
        num_neurons: int,
        activation_func: AbstractActivationFunction = None,
        optimizer: Optional[AbstractOptimizer] = None,
    ):
        """Initializer of an abstract layer object."""
        self._activation_func = activation_func
        self.params: Dict[str, Optional[np.ndarray]] = {}
        self.num_neurons = num_neurons
        self.optimizer = optimizer
        self.z_val: np.ndarray = np.zeros((0, 0))

    def __repr__(self):
        param_str = f", params: W: {self.params['W'].shape}, b: {self.params['b'].shape}" if self.params else ""
        return (
            f"{self.__class__.__name__}: num_neurons = {self.num_neurons}, activation = {type(self._activation_func)}"
            + param_str
        )

    @property
    def parameters(self):
        return self.params

    @property
    def activation_func_type(self) -> str:
        return type(self._activation_func)

    @property
    def activation_func(self) -> AbstractActivationFunction:
        return self._activation_func

    def initialize_params(self, num_neuron_prev_layer: int):
        """Initializes W,b using `num_neuron_prev_layer` be the number of neurons from the previous layer"""
        raise NotImplementedError

    def propagate_forward(self, a_prev: np.ndarray) -> np.ndarray:
        """Computes the (activation) output for 1 layer given the activation input from the previous layer.

        This function also update the hidden variable as well.
        Args:
            a_prev: The input data of the current layer. This data is also the activation output of the layer before it.
        Returns:
            A matrix of size (n^[l], m) for the activation output.
        """
        self.z_val, a_val = self._propagate_forward(a_prev)
        return a_val

    def propagate_forward_without_updating_cache(self, a_prev: np.ndarray) -> np.ndarray:
        """Computes the activation output for 1 layer given the activation input from the previous layer.

        This function does NOT update the hidden variable and the drop out mask.
        Args:
            a_prev: The input data of the current layer. This data is also the activation output of the layer before it.
        Returns:
            A matrix of size (n^[l], m) for the activation output.
        """
        _, a_val = self._propagate_forward(
            a_prev,
        )
        return a_val

    @classmethod
    def _propagate_forward(
        cls,
        a_prev: np.ndarray,
    ) -> Tuple:
        """Computes A^[l], Z^[l], and the drop out mask from the input A^[l-1] (a_val).

        Args:
            a_prev: The input data from the prev. layer. This is a matrix of size (n^[l-1], m)
        Returns:
            A tuple of (Z^[l], A^[l]).
        """
        raise NotImplementedError

    def update_params(self, learning_rate: float):
        """Updates the parameters of the network

        The updating formula is
            param := param - learning_rate * dparams for params in self.grads
        """
        raise NotImplementedError
