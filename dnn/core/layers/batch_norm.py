from typing import Optional

import numpy as np

from dnn.core.layers.layer import BaseLayer
from dnn.core.layers.layer import ForwardPropagationMode


class BatchNormLayer(BaseLayer):
    """The Batch normalization that is inserted after computing the linear hidden units but before the activation.

    This function is too be used with the F
    Reference:
        [1]. Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift
        [2]. https://www.youtube.com/watch?v=jhUZ800C650&index=5&list=PLLvH2FwAQhnpj1WEB-jHmPuUeQ8mX-XXG&ab_channel=Seong

    Args:
        num_neurons: The number of neurons in the current layer. It is equal to n_x
        eps: A small floating point number used to avoid zero division.
    Attributes:
        train_mu: A (n_x, 1) vector for the mean over m samples.
        train_var: A (n_x, 1) vector for un-bias variance across m samples (= sum over i (x^(i) - train_mu)/ (m-1) *
    """

    def __init__(self, num_neurons: float, eps=1e-8):
        self._num_neurons = num_neurons
        self.params = {"gamma": np.ones((self._num_neurons, 1)), "beta": np.zeros((self._num_neurons, 1))}
        self._m_vals = []
        self._mu_batch_vals = []
        self._var_batch_vals = []
        self._var_batch = None
        self._mu_batch = None
        self._eps = eps
        self.train_mu: Optional[np.ndarray] = None
        self.train_var: Optional[np.ndarray] = None
        self.grads = None

    def propagate_forward(self, z_val: np.ndarray, propagation_mode=ForwardPropagationMode.Train) -> np.ndarray:
        """Computes y^[l] from z^[l].

        Algorithm:
            1) Compute the mean and the variance from the minibatch.
            2) Normalize into a new variable x^hat[l] that has zero-mean and unit deviation.
            3) Multiply x^hat[l] by the `gamma` and add by the `beta` parameter. Each is a vector of size (n_x, 1)
        Args:
            z_val: A numpy array of size (n_x, m).
            propagation_mode (optional): The training mode of the layer. Defaults to ForwardPropagationMode.Train.
        """
        if not isinstance(propagation_mode, ForwardPropagationMode):
            raise ValueError("The propagation_mode mode must be an instance of ForwardPropagationMode!")
        if propagation_mode == ForwardPropagationMode.Train:
            self._mu_batch = np.mean(z_val, axis=1, keepdims=True)
            self._var_batch = np.var(z_val, axis=1, keepdims=True)
            self._x_hat = (z_val - self._mu_batch) / np.sqrt(self._var_batch + self._eps)
            self._m_batch = z_val.shape[1]

            # Add batch training result to the the cache
            self._m_vals.append(self._m_batch)
            self._mu_batch_vals.append(self._mu_batch)
            self._var_batch_vals.append(self._var_batch)
        if propagation_mode == ForwardPropagationMode.Predict:
            self._m_vals = np.array(self._m_vals)
            self._mu_batch_vals = np.squeeze(np.array(self._mu_batch_vals)).T
            self._var_batch_vals = np.squeeze(np.array(self._var_batch_vals)).T
            m = np.sum(self._m_vals)
            self.train_mu = np.sum(np.multiply(self._m_vals, self._mu_batch_vals), axis=1, keepdims=True) / m
            self.train_var = np.sum(np.multiply(self._m_vals, self._var_batch_vals), axis=1, keepdims=True) / (m - 1)

            # Normalize the sample
            self._x_hat = (z_val - self.train_mu) / np.sqrt(self.train_var + self._eps)
        return self._x_hat * self.params["gamma"] + self.params["beta"]

    def propagate_backward(self, dy_val: np.ndarray) -> np.ndarray:
        """Computes dz^[l] (derivative of the input) from dy^[l] (derivative of the output) and dgamma and dbeta.

        This layer is typically after FC-> BN -> Non-linearity -> FC -> BN -> ... Therefore, the input is dz^[l].
        For simplicity, we will ignore l in the derivation below.
        Note that we have 4 key equations:
            1) mu = 1 /m  * sum over samples (z_(i))
            2) var = 1 / m * sum over samples (z_(i) - mu) ** 2
            3) x_hat_(i) = (z_i - mu) / (var + eps) ** 0.5
            4) y^[l] = gamma * x_hat^[l] + beta

        For the computing graph, we have:
            {z_(i)} -> mu
                       var
                       x_hat(i)

            mu -> {x_hat_(i)}
                  var

            var -> x_hat_(i)
            {x_hat_(i)} -> {y_(i)}
            gamma -> y^[l]
            beta -> y^[l]

        Hence:
            1) dx_hat_(i) = dy_(i) * dy_(i) over dx_hat(i) = gamma * dy_(i)
               or in matrix form: dX_hat = gamma * dY.

            2) dvar = sum over i {dx_hat_(i) * dx_hat(i) over dvar} =
                -0.5 * sum over i {dx_hat_(i) * (x^(l)- mu) / (var + eps) ** 1.5} =
                -0.5 * sum over i {dx_hat_(i) * x_hat_(i)} / (var + eps)
                or in matrix form: dvar = -0.5 * np.sum(dX_hat * X_hat, axis = 1) / (var + eps)

            3) dmu = sum over i (dx_hat_(i) * dx_hat_(i) over mu) + dvar * dvar over mu
                = -sum over i {dx_hat_(i) / (var + eps) ** 0.5 + dvar * (1/m) * 2 * (z_(i) - mu)}
               or in matrix form:
                dmu = -np.sum(dx_hat, axis = 1) / (var + eps) ** 0.5
                    - dvar * np.sum(z - mu, axis = 1) * 2 / m

            4) dz_(i) = dmu * dmu over z_(i) + dvar * dvar over z_(i) + dx_hat_(i) * dx_hat_(i) over dz_(i)
                = dmu * (1 / m) + dvar * (1 / m) * 2 * (z_(i) - mu) + dx_hat(i) / (var + eps) ** 0.5
               or in matrix form:
               dz = 1 / m * dmu + 2 / m * (z - mu) + dx_hat / (var + eps) ** 0.5

            5) dgamma = sum over i {dy_i * dy_i over gamma}
                = sum over i {dy_i * x_hat_(i)}

            6) dbeta = sum over i {dy_i * dy_i over beta}
                = sum over i {dy_i}

        Reference:
            Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift.

        Args:
            dy_val: A numpy array of size (n_X, m) for the derivative of the cost function w.r.t y^[l].
        """
        dx_hat = self.params["gamma"] * dy_val
        dvar = -0.5 * np.sum(dx_hat * self._x_hat, axis=1, keepdims=True) / (self._var_batch + self._eps)
        std_val = np.sqrt(self._var_batch + self._eps)
        z_minus_mu = self._x_hat * std_val
        dmu = -np.sum(dx_hat, axis=1, keepdims=True) / std_val - 2 / self._m_batch * dvar * np.sum(
            z_minus_mu, axis=1, keepdims=True
        )
        dz = dmu / self._m_batch + 2 / self._m_batch * z_minus_mu + dx_hat / std_val
        dgamma = np.sum(dy_val * self._x_hat, axis=1, keepdims=True)
        dbeta = np.sum(dy_val, axis=1, keepdims=True)
        self.grads = {"dgamma": dgamma, "dbeta": dbeta}
        return dz
