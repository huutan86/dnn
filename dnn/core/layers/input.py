from typing import Tuple

import numpy as np

from dnn.core.layers.layer import BaseLayer


class InputLayer(BaseLayer):
    """The input layer of the network.

    Args:
        num_neurons: The number of neurons of this layer.
    """

    def __init__(self, num_neurons: int):
        """Initializer for an input layer."""
        super().__init__(num_neurons=num_neurons, activation_func=None)

    @classmethod
    def _propagate_forward(
        cls,
        a_prev: np.ndarray,
    ) -> Tuple:
        return a_prev, a_prev
