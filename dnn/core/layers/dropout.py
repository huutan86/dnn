import numpy as np

from dnn.core.layers.layer import BaseLayer


class DropoutLayer(BaseLayer):
    """The dropout layer suggested in the paper: Dropout: A Simple Way to Prevent Neural Networks from Overfitting.

    Args:
        keep_prob (optional): The probability of the neuron is kept if drop out is used. Defaults to 1, meaning drop
            out is not used.
    """

    def __init__(self, keep_prob: float = 1.0):
        self._keep_prob = keep_prob
        self.dropout_mask = None

    def __repr__(self):
        return f"{self.__class__.__name__}: keep_prob = {self._keep_prob}"

    @property
    def keep_prob(self) -> float:
        return self._keep_prob

    def propagate_forward(self, a_prev: np.ndarray) -> np.ndarray:
        """Computes the D^[l] drop-out mask and inversely scales the activation output.

        Args:
            a_prev: The input for the drop out layer. This matrix has size of (n^[l], m)
        Returns:
            A tuple the mask used for drop out and the inversely scaled activation outputs.
        """
        mask = np.random.rand(a_prev.shape[0], a_prev.shape[1])
        self.dropout_mask = (mask < self._keep_prob).astype(int)
        return a_prev * self.dropout_mask / self._keep_prob

    def propagate_backward(
        self,
        da_val: np.ndarray,
    ) -> np.ndarray:
        """Masks the input dA^[l] (da_val) with the 'mask' and scales it by 1/keep_prob before propagating it back.

        Returns:
            dA^[l] that has been accounted for drop out.
        """
        if self._keep_prob == 1.0:
            return da_val
        return da_val * self.dropout_mask / self._keep_prob
