"""Implemetation of the convolutional layer in the network."""
from typing import Optional
from typing import Tuple

import numpy as np
from scipy.signal import correlate2d

from .layer import BaseLayer
from dnn.core.optimization import AbstractOptimizer
from dnn.core.optimization import AdamOptimizer


class Convolutional(BaseLayer):
    """Implementation of the convolutional layer.

    References:
        [1]. https://cs231n.github.io/convolutional-networks/

    Args:
        filter_size: The size of the filter. This is a tuple of 4 elements [f, f, n_c_prev, n_c] where f is the size of
            the filter kernel, n_c_prev, n_c: the number of channels in the previous layer and the current layers.
        stride (optional): A number that specifies the stride in the row and column dimension of the filter.
        pad (optional): A number that specifies the  number of elements to pad in the X and Y dimension. The padded
            image will have size of (n_h + 2 * pad_size, n_w + 2 * pad_size, n_c_prev)
        optimizer (optional): The type of the optimizer to use. This affects how we update the parameters. Defaults
            to Adam.
    """

    def __init__(
        self,
        filter_size: Tuple[int],
        stride: int = 1,
        pad=1,
        optimizer: AbstractOptimizer = AdamOptimizer(),
    ):
        self.optimizer = optimizer
        self.z_val: np.ndarray = np.zeros((0, 0))
        self._filter_size = filter_size

        if self._filter_size[0] != self._filter_size[1]:
            raise ValueError("The first two dimensions of the filter size must be similar.")
        self._f = self._filter_size[0]

        self._s = stride
        self._p = pad
        self._n_h, self._n_w, self._n_c_prev, self._n_c = self._filter_size
        self.initialize_params()

    def __repr__(self):
        return (
            f"{self.__class__.__name__}: filter size = {self._filter_size}, pad size = {self._p}, stride = "
            f"{self._s}.."
        )

    def initialize_params(self) -> None:
        """Initializes the parameters of the current layer.

        The He initiializaion can be found at
        https://medium.com/@tylernisonoff/weight-initialization-for-cnns-a-deep-dive-into-he-initialization-50b03f37f53d
        """
        self.params = {}
        nl = self._f * self._f * self._n_c_prev  # This is the number of terms that affects the calculation of variance.
        self.params["W"] = np.sqrt(2 / nl) * np.random.randn(self._f, self._f, self._n_c_prev, self._n_c)
        self.params["b"] = np.zeros(shape=(1, 1, 1, self._n_c))

    def propagate_forward(self, a_prev: np.ndarray) -> np.ndarray:
        """Computes the output of the convolutional layer.

        The output will be calculated as:
            Z[i, h, w, c] = sum_over_prev_c{a_prev[i, ..., prev_c] x W[f, f, prev_c, c])}[::s, ::s] + b[0, 0, 0, c].
        where i in range(m), which is the sample index, c is in range(n_c) which defines the output channel index.
        prev_c is the index of the input channel.

        Args:
            a_prev: A numpy array of dimension [m, n_h_prev, n_w_prev, n_c_prev] of the activation output of the
                previous layer. Here, m is the number of samples in the batch. n_h_prev, n_w_prev, n_c_prev are the
                number of rows, number of columns, and number of channels of the activation for the previous layer.

        Return:
            A numpy array for Z[i, h, w, c] of dimensions [m, n_h, n_w, n_c] where
                n_h = floor((n_h_prev + 2 * pad - f) / stride + 1)
                n_w = floor((n_w_prev + 2 * pad - f) / stride + 1).
                n_c is the number of filters, also the nummber of output channel.
        """
        if self._n_c_prev != a_prev.shape[3]:
            raise ValueError("The last dimension of the input and the 3rd dimension of filter size does not match!!!")
        m, n_h_prev, n_w_prev, _ = a_prev.shape

        # Output dimensions after convolution.
        n_h = self._calculate_new_1d_dimension(n_h_prev, self._p, self._f, self._s)
        n_w = self._calculate_new_1d_dimension(n_w_prev, self._p, self._f, self._s)
        z = np.zeros((m, n_h, n_w, self._n_c), dtype=np.float32)

        # Pad the data
        a_prev_padded = np.pad(
            a_prev, ((0, 0), (self._p, self._p), (self._p, self._p), (0, 0)), mode="constant", constant_values=(0, 0)
        )

        for sample in range(m):
            for out_chan in range(self._n_c):
                z[sample, :, :, out_chan] = self._generate_one_channel_filter_response(
                    a_prev=a_prev_padded[sample, ...],
                    w=self.params["W"][:, :, :, out_chan],
                    b=self.params["b"][0, 0, 0, out_chan],
                    n_h=n_h,
                    n_w=n_w,
                )
        return z

    @staticmethod
    def _calculate_new_1d_dimension(prev_dim: int, padd_size: int, kernel_size: int, stride: int) -> int:
        return int((prev_dim + 2 * padd_size - kernel_size) / stride) + 1

    def _generate_one_channel_filter_response(
        self, a_prev: np.ndarray, w: np.ndarray, b: np.ndarray, n_h: int, n_w: int
    ) -> np.ndarray:
        """Generates the response of one channel filter for 1 output channel of 1 sample.

        The output will be calculated as:
            Z[h, w, c] = sum_over_prev_c{a_prev[..., prev_c] x W[f, f, prev_c, c])}[::s, ::s] + b[0, 0, 0, c].
        where i in range(m), which is the sample index, c is in range(n_c) which defines the output channel index.
        prev_c is the index of the input channel.

        Args:
            a_prev: The activation output of the previous layer of shape (n_h_prev, n_w_prev, n_c_prev) for 1 sample.
            w: The filtering kernel of shape (f, f, n_c_prev)
            b: The scalar coefficient for the offset

        Returns:
            A matrix Z of size (n_h, n_w) which is the response of the filter for the current input.
        """
        n_c_prev = a_prev.shape[-1]
        correlation_outputs_all_channels = np.zeros((n_h, n_w, self._n_c_prev))
        for prev_chan in range(n_c_prev):
            correlation_outputs_all_channels[:, :, prev_chan] = correlate2d(
                a_prev[:, :, prev_chan], w[:, :, prev_chan], mode="valid"
            )[:: self._s, :: self._s]
        return np.sum(correlation_outputs_all_channels, axis=2) + b
