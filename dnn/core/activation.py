"""Provides functionality to define activation function.

** Tan H. Nguyen **
"""
from abc import ABC
from abc import abstractmethod

import numpy as np


class AbstractActivationFunction(ABC):
    """Abstract class for the activation function."""

    @abstractmethod
    def propagate_forward(self, z: np.ndarray) -> np.ndarray:
        """Computes the activation output from the hidden unit.

        Args:
            z: A numpy array of size (n^[l], m) where n^[l] is the number of activation output, m is the number of
                samples.

        Returns:
            A numpy array of the same size.
        """
        raise NotImplementedError

    @abstractmethod
    def propagate_backward(self, da_val: np.ndarray, z_val: np.ndarray) -> np.ndarray:
        """Computes the dz output from the hidden unit.

        Args:
            da_val: A numpy array of size (n^[l], m) where n^[l] is the number of output nodes, m is the number of
                samples.
            z_val: A numpy array for z^[l] of size (n^[l], m)
        Returns:
            A numpy array of the same size.
        """
        raise NotImplementedError


class ReLUActivationFunction(AbstractActivationFunction):
    """ReLU activation function."""

    def propagate_forward(self, z: np.ndarray) -> np.ndarray:
        return np.clip(z, 0, np.inf)

    def propagate_backward(self, da_val: np.ndarray, z_val: np.ndarray) -> np.ndarray:
        assert da_val.shape == z_val.shape
        dg_over_dz = np.ones_like(da_val)
        dg_over_dz[z_val <= 0] = 0
        return np.multiply(da_val, dg_over_dz)


class SigmoidActivationFunction(AbstractActivationFunction):
    """Sigmoid activation function."""

    def propagate_forward(self, z: np.ndarray) -> np.ndarray:
        return np.divide(1, (1 + np.exp(-z)))

    def propagate_backward(self, da_val: np.ndarray, z_val: np.ndarray) -> np.ndarray:
        """
        Computes the dz output from the hidden unit.

        Because g(z) = 1/(1 + e^-z), g'(z) = g(1-g).
        Args:
            da_val: A numpy array of size (n^[l], m) where n^[l] is the number of output nodes, m is the number of
                samples.
            z_val: A numpy array for z^[l] of size (n^[l], m)
        Returns:
            A numpy array of the same size.
        """
        assert da_val.shape == z_val.shape
        dg_over_dz = self.propagate_forward(z_val) * (1 - self.propagate_forward(z_val))
        return np.multiply(da_val, dg_over_dz)


class SoftmaxActivationFunction(AbstractActivationFunction):
    """Softmax activation function."""

    def propagate_forward(self, z: np.ndarray) -> np.ndarray:
        temp = np.exp(z)
        return np.divide(temp, np.sum(temp, axis=0))

    def propagate_backward(self, da_val: np.ndarray, z_val: np.ndarray) -> np.ndarray:
        """Computes the dz output from the hidden unit.

        Because a_l = g_l(z_1,...) = exp(z_l) / sum over k1 [exp(z_k1)] where k is the node index. Hence,
        dz_k = sum over l {da_l * g_l'(z_k)} where the derivative g_l'(z_k) depends on l = k and l!=k
        If l = k:
            g'_k(z_k) = {exp(z_k) *  sum over k1 [exp(z_k1)] - exp(z_k) * exp(z_k)}/ {sum over k1 [exp(z_k1)]}^2
                = g(z_k) * (1 - g(z_k))
        else:
            g'_l(z_k) = -exp(z_l)exp(z_k){sum over k1 [exp(z_k1)]}^2 =  - g(z_l) * g(z_k).
        Hence,
            dz_k = g(z_k) * {da_k - sum over l {da_l * g(z_l)}} = g(z_k) * (da_k - const)
            where const does not depend on the node index. However, it depends on the sample index. For each sample,
            this is a const term.
        Args:
            da_val: A numpy array of size (n^[l], m) where n^[l] is the number of output nodes, m is the number of
                samples.
            z_val: A numpy array for z^[l] of size (n^[l], m)
        Returns:
            A numpy array of the same size.
        """
        assert da_val.shape == z_val.shape
        g_of_zk = self.propagate_forward(z_val)
        const_val = np.sum(np.multiply(da_val, g_of_zk), axis=0)
        return np.multiply(g_of_zk, da_val - const_val)
