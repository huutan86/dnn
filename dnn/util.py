"""Provides utilities for DNN.

** Tan H. Nguyen **
"""
from typing import List
from typing import Tuple

import numpy as np


def accuracy(y_predict: np.ndarray, y_groundtruth: np.ndarray) -> float:
    """Computes the accuracy between the prediction and the groundtruth. Each is a (1, m) vector."""
    if not y_predict.shape == y_groundtruth.shape:
        raise ValueError("Cannot compute the accuracy because y_predict and y_groundtruth are not size-consistent!")
    return len(np.where(y_predict == y_groundtruth)[0]) / y_groundtruth.shape[1]


def random_mini_batches(
    x: np.ndarray, y: np.ndarray, mini_batch_size: int = 64, seed: int = 0
) -> List[Tuple[np.ndarray, np.ndarray]]:
    """Returns a list of tuples, each tuple includes a mini_batch_X and mini_batch_Y.
    Args:
        x: A (n_x, m) numpy array of all the data.
        y: A (1, m) numpy array of the label.
        mini_batch_size (optional): The size of the mini batch to use. Defaults to 64.
        seed (optional): A number for initializing the random number generator.
    """
    np.random.seed(seed)
    if x.shape[1] != y.shape[1]:
        raise RuntimeError("The number of columns of X and Y must be compatible!")
    m = x.shape[1]
    mini_batches = []
    # Permutes all the samples by randomizing the index.
    permuted_indices = np.random.permutation(m)
    # Handles completed mini-batches
    for mini_batch in range(m // mini_batch_size):
        permuted_mini_batch_idx = permuted_indices[mini_batch * mini_batch_size : (mini_batch + 1) * mini_batch_size]
        mini_batches.append((x[:, permuted_mini_batch_idx], y[:, permuted_mini_batch_idx]))

    if m % mini_batch_size != 0:
        permuted_mini_batch_idx = permuted_indices[(m // mini_batch_size) * mini_batch_size :]
        mini_batches.append((x[:, permuted_mini_batch_idx], y[:, permuted_mini_batch_idx]))
    return mini_batches


def convert_to_one_hot(labels: np.ndarray, num_classes: int) -> np.ndarray:
    """Generates a (C, m) numpy array whose every column has exactly one entry of 1 indicating the the class type.

    Args:
        labels: A (1, m) numpy array that indicates the class of the sample.
        num_classes: The number of classes (C).
    """
    m = labels.shape[1]
    out_mat = np.zeros((num_classes, m), dtype=np.float)
    for sample in range(m):
        out_mat[labels[0, sample], sample] = 1
    return out_mat
