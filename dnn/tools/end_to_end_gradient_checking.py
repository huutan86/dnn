"""Provides functionality to check the gradients of the a neural network (through all layers)."""
from typing import Dict
from typing import Tuple

import numpy as np
from absl import logging

from dnn import DNN
from dnn.core import InputLayer
from dnn.core.loss import compute_total_loss


class EndToEndGradientChecker:
    """A class that check the gradients of different layers of a deep network.

    Args:
        net: A neural network that we want to check the gradient computation.
    """

    def __init__(self, net: DNN):
        self._net = net

    def check(self, x: np.ndarray, y: np.ndarray) -> None:
        """Checks the validity of the gradient for different layers.

        Args:
            x: A numpy array for input data of size (n_x, m)
            y: A numpy array of 0 and 1 for the output data. It has a dimension of (1, m) for binary classification and
                (C, m) for multi-class classification.
        """
        # Compute the gradients from back propagation
        y_hat = self._net.propagate_forward(x)
        self._net.propagate_backward(yhat_val=y_hat, y_val=y)
        for idx, layer in enumerate(self._net.layers):
            if not isinstance(layer, InputLayer):
                logging.info(f"Checking the gradient of layer {layer}")
                self._check_gradient(layer_index=3, x=x, y=y)

    def _check_gradient(self, layer_index: int, x: np.ndarray, y: np.ndarray):
        """Checks the gradients between the backprop and from the cost function."""

        def _is_good_params(a: np.ndarray, b: np.ndarray, thresh_val=1e-6) -> Tuple[bool, float]:
            temp = np.linalg.norm(a - b) / (np.linalg.norm(a) + np.linalg.norm(b))
            if temp < thresh_val:
                return True, temp
            return False, temp

        # Check the gradients dw and db.
        bp_grads = self._net.layers[layer_index].grads
        expected = self._compute_dw_and_db(layer_index, x=x, y=y)
        for param in bp_grads:
            res, val = _is_good_params(bp_grads[param], expected[param], thresh_val=1e-6)
            if res:
                logging.info(f"Layer {layer_index}, gradient of {param} is ok. Congrats!")
            else:
                raise RuntimeError(
                    f"Layer {layer_index}, gradient of {param} is NOT ok because value = "
                    f"{bp_grads[param]}, expected = {expected[param]}, error = {val}. "
                    f"Please check it again!"
                )
        """# Check the gradients dz
        dz = self._net.layers[layer_index].dz_val
        expected_dz = self._compute_dz(layer_index, y=y)
        res, val = _is_good_params(dz, expected_dz, thresh_val=1e-6)
        if res:
            logging.info(f"Layer {layer_index}, gradient of dz is ok. Congrats!")
        else:
            raise RuntimeError(
                f"Layer {layer_index}, gradient of dz is NOT ok because dz = "
                f"{dz}, expected dz = {expected_dz}, error = {val}. "
                f"Please check it again!"
            )
        """

    def _compute_dz(self, layer_index: int, y: np.ndarray, epsilon: float = 1e-7) -> np.ndarray:
        """Computes the dz of the layer with the index of `layer_index`."""
        z_val = np.copy(self._net.layers[layer_index].z_val)

        def _propagate_and_compute_loss(zval: np.ndarray, idx: int, y: np.ndarray) -> float:
            """Propagates from the hidden variable of the current layer to the output layer, then, computes the cost.

            Args:
                z_val: A numpy array in which 1 sample was perturbed by adding an epsilon to 1 one element of the its
                    original value.
            """
            # Propagate through the activation of the current layer.
            layer = self._net.layers[idx]
            a_val = layer.activation_func.propagate_forward(zval)
            # Propagate through the rest of the network if this is not the last layer. No need to cache because
            # we don't need to the backward pass.
            for layer_id in range(idx + 1, len(self._net.layers)):
                a_val = self._net.layers[layer_id].propagate_forward(a_val)

            return self._cost(a_val, y=y) * y.shape[1]  # Note that loss function is not normalized by 1/m, we need
            # to compensate that.

        rows, cols = z_val.shape
        dz = np.zeros_like(z_val)
        for r in range(rows):
            for c in range(cols):
                z_plus = np.copy(z_val)
                z_plus[r][c] = z_plus[r][c] + epsilon
                j_plus = _propagate_and_compute_loss(z_plus, idx=layer_index, y=y)
                z_min = np.copy(z_val)
                z_min[r][c] = z_min[r][c] - epsilon
                j_min = _propagate_and_compute_loss(z_min, idx=layer_index, y=y)
                dz[r][c] = (j_plus - j_min) / (2 * epsilon)

                # Restore the network status.
                _ = _propagate_and_compute_loss(z_val, idx=layer_index, y=y)

        return dz

    def _compute_dw_and_db(
        self, layer_index: int, x: np.ndarray, y: np.ndarray, epsilon: float = 1e-7
    ) -> Dict[str, np.ndarray]:
        """Computes the derivative of the cost function w.r.t w and b."""
        params = self._net.layers[layer_index].params
        w = np.copy(params["W"])
        b = np.copy(params["b"])

        rows, cols = w.shape
        dw = np.zeros_like(w)
        db = np.zeros_like(b)
        for r in range(rows):
            for c in range(cols):
                w_plus = np.copy(w)
                w_plus[r][c] = w_plus[r][c] + epsilon
                self._net.layers[layer_index].params["W"] = w_plus
                j_plus = self._cost(y_hat=self._net.propagate_forward(x), y=y)

                w_minus = np.copy(w)
                w_minus[r][c] = w_minus[r][c] - epsilon
                self._net.layers[layer_index].params["W"] = w_minus
                j_min = self._cost(y_hat=self._net.propagate_forward(x), y=y)
                dw[r][c] = (j_plus - j_min) / (2 * epsilon)

                # Restore w before moving on to a different parameters.
                self._net.layers[layer_index].params["W"] = w

        for r in range(rows):
            b_plus = np.copy(b)
            b_plus[r][0] = b_plus[r][0] + epsilon
            self._net.layers[layer_index].params["b"] = b_plus
            j_plus = self._cost(y_hat=self._net.propagate_forward(x), y=y)

            b_min = np.copy(b)
            b_min[r][0] = b_min[r][0] - epsilon
            self._net.layers[layer_index].params["b"] = b_min
            j_min = self._cost(y_hat=self._net.propagate_forward(x), y=y)
            db[r][0] = (j_plus - j_min) / (2 * epsilon)

            # Restore parameters and move on.
            self._net.layers[layer_index].params["b"] = b
        return {"dw": dw, "db": db}

    def _cost(self, y_hat: np.ndarray, y: np.ndarray) -> float:
        """Computes the value of the cost function (normalized by the number of samples)."""
        m = y_hat.shape[1]
        reg_cost, non_reg_cost = compute_total_loss(
            y_hat, y, loss_type=self._net.loss_type, network_params=self._net.parameters, l2_lambd=self._net.l2_lambda
        )
        return (reg_cost + non_reg_cost) / m
