"""Provides the utilities for gradient checking."""
import numpy as np


def compute_gradient(f, x0: np.ndarray, eps: float = 1e-7) -> np.ndarray:
    """Computes the numerical derivative of a function f(x) at x0.

    Args:
        f: A function object that takes a single input argument x as the input.
        x0: A numpy array at which we want to compute the gradient at.
        eps: A small constant that we used to compute the gradient
    Returns:
        The numerical value of the gradient that is (f(x0 + eps) - f(x0 - eps)) / (2 * eps)
    """
    rows, cols = x0.shape
    grads_val = np.zeros_like(x0)
    for r in range(rows):
        for c in range(cols):
            x0_plus = np.copy(x0)
            x0_plus[r][c] = x0_plus[r][c] + eps
            x0_minus = np.copy(x0)
            x0_minus[r][c] = x0_minus[r][c] - eps
            grads_val[r][c] = (f(x0_plus) - f(x0_minus)) / (2 * eps)
    return grads_val
