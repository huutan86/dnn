"""Provides functionality to load the training and testing data for cat/non-cat classification."""
from os import path
from pathlib import Path
from typing import List
from typing import Tuple

import h5py
import matplotlib.pyplot as plt
import numpy as np
import sklearn.datasets

from dnn import DNN

_data_folder = path.join(Path(__file__).parent.parent, "datafolder")


def _load_data(train_file_name: str, test_file_name: str) -> Tuple[np.ndarray]:
    train_dataset = h5py.File(path.join(_data_folder, train_file_name), "r")
    train_set_x = np.array(train_dataset["train_set_x"][:])  # Features
    train_set_y = np.array(train_dataset["train_set_y"][:])  # Labels

    test_dataset = h5py.File(path.join(_data_folder, test_file_name), "r")
    test_set_x = np.array(test_dataset["test_set_x"][:])
    test_set_y = np.array(test_dataset["test_set_y"][:])

    classes = np.array(test_dataset["list_classes"][:])
    return train_set_x, train_set_y, test_set_x, test_set_y, classes


def load_cat_data() -> Tuple[np.ndarray]:
    """Loads the cat/non-cat training data."""
    return _load_data(train_file_name="train_catvnoncat.h5", test_file_name="test_catvnoncat.h5")


def load_sign_data() -> Tuple[np.ndarray]:
    """Loads the sign data sets for training and testing."""
    return _load_data(train_file_name="train_signs.h5", test_file_name="test_signs.h5")


def load_2d_data() -> Tuple[np.ndarray, np.ndarray]:
    """Loads the 2d data for evaluating different optimizer"""
    np.random.seed(3)
    train_x, train_y = sklearn.datasets.make_moons(n_samples=300, noise=0.2)
    # Plot the data
    plt.figure(1)
    plt.scatter(train_x[:, 0], train_x[:, 1], c=train_y, s=40, cmap=plt.cm.Spectral)
    plt.pause(0.01)

    return train_x.T, train_y.reshape((1, -1))


def display_training_examples(
    training_data: Tuple[np.ndarray], classes: List[np.ndarray], num_examples: int = 10
) -> None:
    """Displays some images of the training example for each class.

    Args:
        training_data: A tuple of (training features, training labels).
        classes: A list of class labels, indexed by the training label.
        num_examples (optional): The number of images to display in each category. Defaults to 10.
    """
    training_x, training_y = training_data
    num_rows, num_cols, num_chans = training_x[0].shape
    all_labels = np.unique(training_y)
    plt.figure(None, figsize=(8, 8))
    for label_index, label in enumerate(all_labels):
        combined_image = np.zeros((num_rows * 2, num_cols * num_examples // 2, num_chans), dtype=training_x[0].dtype)
        class_images = training_x[training_y == label]
        for image_index_row in range(2):
            for image_index_col in range(num_examples // 2):
                combined_image[
                    image_index_row * num_rows : (image_index_row + 1) * num_rows,
                    image_index_col * num_cols : (image_index_col + 1) * num_cols,
                    :,
                ] = class_images[image_index_row * num_examples // 2 + image_index_col]
        plt.subplot(len(all_labels), 1, label_index + 1)
        plt.imshow(combined_image)
        plt.title(f"Class {label} images.")
    plt.draw_all()
    plt.pause(0.05)


def display_misclassified_examples(
    test_data: Tuple[np.ndarray], predicted_y: np.ndarray, classes: List[np.ndarray], num_images_to_display=20
):
    """Display images that are mislabeled."""
    testing_x, testing_y = test_data
    testing_y = np.squeeze(testing_y)
    predicted_y = np.squeeze(predicted_y)
    wrongly_classified_indices = np.squeeze(np.asarray(np.where(testing_y != predicted_y)))
    num_images = min(num_images_to_display, len(wrongly_classified_indices))
    plt.figure(None, figsize=(10, 10))
    for index in range(num_images):
        # Get the image in the original dataset
        image_index = wrongly_classified_indices[index]
        plt.subplot(3, np.ceil(num_images / 3), index + 1)
        plt.imshow(testing_x[image_index])
        plt.title(
            f"Predict: {classes[int(predicted_y[image_index])].decode('utf-8')} \n, "
            f"Class: {classes[testing_y[image_index]].decode('utf-8')}"
        )
    plt.draw_all()
    plt.pause(0.05)


def plot_decision_boundary(model: DNN, x: np.ndarray, y: np.ndarray) -> None:
    """Plots a decision boundary between different classes.

    Args:
        model: the trained neural network.
        x: A (n_x, m) numpy array for the feature vector.
        y: A (1, m) numpy array for the label.
    """
    x_min, x_max = x[0, :].min() - 0.5, x[0, :].max() + 0.5
    y_min, y_max = x[1, :].min() - 0.5, x[1, :].max() + 0.5
    step_size = 0.01
    xx, yy = np.meshgrid(np.arange(x_min, x_max, step_size), np.arange(y_min, y_max, step_size))
    pred_label = model.predict(np.c_[xx.ravel(), yy.ravel()].T).reshape(xx.shape)

    # Plot the boundary
    plt.figure()
    plt.contourf(xx, yy, pred_label, cmap=plt.cm.Spectral)

    # Just overlay the data on the top.
    plt.scatter(x[0, :], x[1, :], c=y[0, :], cmap=plt.cm.Spectral)
    plt.title("Decision boundary.")
    plt.pause(0.01)
