"""Demo script to train a deep NN.

** Copyright Tan H. Nguyen **
"""
from absl import app
from absl import logging

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.optimization import AdamOptimizer
from dnn.util import accuracy
from script._util import display_misclassified_examples
from script._util import display_training_examples
from script._util import load_cat_data


def main(argv):
    """Simple script that test training and testing a DNN."""
    del argv

    # Load cat/non-cat data.
    train_x_org, train_y_org, test_x_org, test_y_org, classes = load_cat_data()
    train_x = train_x_org.copy()
    train_y = train_y_org.copy()
    test_x = test_x_org.copy()
    test_y = test_y_org.copy()

    display_training_examples(training_data=(train_x_org, train_y_org), classes=classes)

    # Reshape the data into a correct shape of (n_x, m)
    train_x = train_x.reshape(train_x.shape[0], -1).T
    train_y = train_y.reshape(1, -1)
    test_x = test_x.reshape(test_x.shape[0], -1).T
    test_y = test_y.reshape(1, -1)

    # Normalize the data so that the input to the layer is between 0 and 1
    train_x = train_x / 255
    test_x = test_x / 255

    n_x = train_x.shape[0]

    logging.info("Demo source to train and test a DNN")
    net1 = DNN(l2_lambd=0.1)
    net1.add_layer(InputLayer(num_neurons=n_x))
    net1.add_layer(
        FullyConnectedLayer(
            num_neurons=20,
            activation_func=ReLUActivationFunction(),
            optimizer=AdamOptimizer(),
        )
    )
    net1.add_layer(
        FullyConnectedLayer(
            num_neurons=8,
            activation_func=ReLUActivationFunction(),
            optimizer=AdamOptimizer(),
        )
    )
    net1.add_layer(
        FullyConnectedLayer(num_neurons=1, activation_func=SigmoidActivationFunction(), optimizer=AdamOptimizer())
    )
    net1.print_network()

    # Train the neural network, use the test set as the dev set.
    net1.train(
        training_x=train_x,
        training_y=train_y,
        dev_x=test_x,
        dev_y=test_y,
        learning_rate=0.007,
        num_epochs=3000,
        print_cost=True,
        num_epoch_log=100,
        beta1=0.9,
        beta2=0.999,
    )

    # Predict the label for the test set.
    predicted_y = net1.predict(testing_x=test_x)
    logging.info(f"Testing accuracy {accuracy(predicted_y, test_y)}")

    # Draw some misclassified example
    display_misclassified_examples(test_data=(test_x_org, test_y_org), predicted_y=predicted_y, classes=classes)
    logging.info("Done!")


if __name__ == "__main__":
    app.run(main)
