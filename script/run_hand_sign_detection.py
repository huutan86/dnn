"""Demo script to train multi-class classifications."""
from absl import app
from absl import logging

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SoftmaxActivationFunction
from dnn.core.layers.layer import InitializationMethod
from dnn.core.loss import LossType
from dnn.core.optimization import AdamOptimizer
from dnn.util import convert_to_one_hot
from script._util import display_training_examples
from script._util import load_sign_data


def main(argv):
    del argv
    # Load sign data
    train_x_org, train_y_org, test_x_org, test_y_org, classes = load_sign_data()
    display_training_examples(training_data=(train_x_org, train_y_org), classes=classes, num_examples=50)
    train_x = train_x_org.copy()
    train_y = train_y_org.copy()
    test_x = test_x_org.copy()
    test_y = test_y_org.copy()
    train_x = train_x.reshape(train_x.shape[0], -1).T
    test_x = test_x.reshape(test_x.shape[0], -1).T

    # Normalize so that the input is from 0 to 1
    train_x = train_x / 255
    test_x = test_x / 255

    train_y = convert_to_one_hot(train_y.reshape(1, -1), num_classes=6)
    test_y = convert_to_one_hot(test_y.reshape(1, -1), num_classes=6)

    # Define the network architecture
    net = DNN(l2_lambd=0.0, initialization_method=InitializationMethod.Xavier, loss_type=LossType.SoftmaxCrossEntropy)
    n_x, m = train_x.shape
    net.add_layer(InputLayer(num_neurons=n_x))
    net.add_layer(
        FullyConnectedLayer(num_neurons=25, activation_func=ReLUActivationFunction(), optimizer=AdamOptimizer())
    )
    net.add_layer(
        FullyConnectedLayer(num_neurons=12, activation_func=ReLUActivationFunction(), optimizer=AdamOptimizer())
    )
    net.add_layer(
        FullyConnectedLayer(
            num_neurons=6,
            activation_func=SoftmaxActivationFunction(),
            optimizer=AdamOptimizer(),
        )
    )

    net.train(
        training_x=train_x,
        training_y=train_y,
        dev_x=None,
        dev_y=None,
        learning_rate=0.0001,
        num_epochs=3000,
        print_cost=True,
        num_epoch_log=1,
        beta1=0.9,
        beta2=0.999,
        mini_batch_size=32,
    )
    logging.info("Done!!!")


if __name__ == "__main__":
    app.run(main)
