"""Demo script to train a deep NN that we.

** Copyright Tan H. Nguyen **
"""
from absl import app
from absl import logging

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.optimization import optimizer
from dnn.core.optimization import OptimizerType
from script._util import load_2d_data
from script._util import plot_decision_boundary


def main(argv):
    """Simple script that test training and testing a DNN."""
    del argv

    # Load cat/non-cat data.
    train_x, train_y = load_2d_data()

    # Normalize the data so that the input to the layer is between 0 and 1
    logging.info("Demo source to train and test a DNN")
    optimizer_type = OptimizerType.Adam
    n_x = train_x.shape[0]
    net1 = DNN(l2_lambd=0.0)
    net1.add_layer(InputLayer(num_neurons=n_x))
    net1.add_layer(
        FullyConnectedLayer(
            num_neurons=5, activation_func=ReLUActivationFunction(), optimizer=optimizer(optimizer_type)
        )
    )
    net1.add_layer(
        FullyConnectedLayer(
            num_neurons=2, activation_func=ReLUActivationFunction(), optimizer=optimizer(optimizer_type)
        )
    )
    net1.add_layer(
        FullyConnectedLayer(
            num_neurons=1, activation_func=SigmoidActivationFunction(), optimizer=optimizer(optimizer_type)
        )
    )
    net1.print_network()

    # Train the neural network, use the test set as the dev set.
    net1.train(
        training_x=train_x,
        training_y=train_y,
        dev_x=None,
        dev_y=None,
        learning_rate=0.0007,
        num_epochs=10000,
        mini_batch_size=64,
        print_cost=True,
        num_epoch_log=1000,
        beta1=0.9,
        beta2=0.999,
        eps=1e-8,
    )

    # Predict the whole training set and plot the decision boundary
    plot_decision_boundary(net1, train_x, train_y)
    logging.info("Done!")


if __name__ == "__main__":
    app.run(main)
