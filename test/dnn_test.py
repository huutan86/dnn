"""Provides tests for the forward propagation of the neural network"""
import numpy as np
import pytest

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.loss import compute_no_regularization_loss
from dnn.core.loss import LossType


class TestForwardPropagation:
    def test_linear_foward(self):
        np.random.seed(1)
        layer = FullyConnectedLayer(num_neurons=3, activation_func=None)
        # Generates data with 2 input samples and a layer of 3 neurons.
        input_val = np.random.randn(3, 2)
        w = np.random.randn(1, 3)
        b = np.random.randn(1, 1)
        z_val = layer.linear_forward(input_val, w, b)
        assert np.linalg.norm(z_val - np.array([[3.26295337, -1.23429987]]), ord="fro") < 1e-5

    def test_layer_output_with_sigmoid_activation(self):
        np.random.seed(2)
        input_val = np.random.randn(3, 2)
        w = np.random.randn(1, 3)
        b = np.random.randn(1, 1)
        layer = FullyConnectedLayer(num_neurons=3, activation_func=SigmoidActivationFunction())
        layer.params["W"] = w
        layer.params["b"] = b
        output_val = layer.propagate_forward(input_val)  # Expected output size (1, 2)
        assert np.linalg.norm(output_val - np.array([[0.96890023, 0.11013289]]), ord="fro") < 1e-5

    def test_layer_output_with_relu_activation(self):
        np.random.seed(2)
        input_val = np.random.randn(3, 2)
        w = np.random.randn(1, 3)
        b = np.random.randn(1, 1)
        layer = FullyConnectedLayer(num_neurons=3, activation_func=ReLUActivationFunction())
        layer.params["W"] = w
        layer.params["b"] = b
        output_val = layer.propagate_forward(input_val)  # Expected output size (1, 2)
        assert np.linalg.norm(output_val - np.array([[3.43896131, 0]]), ord="fro") < 1e-5

    def test_forward_propagation(self):
        np.random.seed(6)
        X = np.random.randn(5, 4)
        W1 = np.random.randn(4, 5)
        b1 = np.random.randn(4, 1)
        W2 = np.random.randn(3, 4)
        b2 = np.random.randn(3, 1)
        W3 = np.random.randn(1, 3)
        b3 = np.random.randn(1, 1)

        net1 = DNN()
        net1.add_layer(InputLayer(num_neurons=X.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=W1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=W2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=W3.shape[0], activation_func=SigmoidActivationFunction()))

        # Fix the parameter for testing
        net1.layers[1].params["W"] = W1
        net1.layers[1].params["b"] = b1
        net1.layers[2].params["W"] = W2
        net1.layers[2].params["b"] = b2
        net1.layers[3].params["W"] = W3
        net1.layers[3].params["b"] = b3

        output_val = net1.propagate_forward(X)
        assert (
            np.linalg.norm(output_val - np.array([[0.03921668, 0.70498921, 0.19734387, 0.04728177]]), ord="fro") < 1e-5
        )


class TestCostFunction:
    def test_cross_entropy_lost(self):
        y = np.asarray([[1, 1, 0]])
        y_hat = np.array([[0.8, 0.9, 0.4]])
        m = y_hat.shape[1]
        val = compute_no_regularization_loss(y_hat, y, loss_type=LossType.LogisticRegressionCrossEntropy) / m
        assert val == pytest.approx(0.2797765635793422, rel=1e-12)


class TestBackwardPropagation:
    def test_compute_da_prev_from_dz(self):
        # Creates a dummy example of 2 samples.
        # The current layer has 3 neurons.
        np.random.seed(1)
        dz = np.random.randn(3, 4)
        # The previous layer has 5 neurons.
        a_prev = np.random.randn(5, 4)
        w = np.random.randn(3, 5)
        # The type of activation function is not important here because we only back propagate through the linear part.
        layer = FullyConnectedLayer(num_neurons=w.shape[0], activation_func=None)
        layer.params["W"] = w
        da_prev, dw, db = layer.linear_backward(dz_val=dz, a_val_prev=a_prev)
        expected_da_prev = np.array(
            [
                [-1.15171336, 0.06718465, -0.3204696, 2.09812712],
                [0.60345879, -3.72508701, 5.81700741, -3.84326836],
                [-0.4319552, -1.30987417, 1.72354705, 0.05070578],
                [-0.38981415, 0.60811244, -1.25938424, 1.47191593],
                [-2.52214926, 2.67882552, -0.67947465, 1.48119548],
            ]
        )
        expected_dw_prev = np.array(
            [
                [0.07313866, -0.0976715, -0.87585828, 0.73763362, 0.00785716],
                [0.85508818, 0.37530413, -0.59912655, 0.71278189, -0.58931808],
                [0.97913304, -0.24376494, -0.08839671, 0.55151192, -0.10290907],
            ]
        )
        assert np.linalg.norm(da_prev - expected_da_prev, ord="fro") < 1e-5
        assert np.linalg.norm(dw - expected_dw_prev, ord="fro") < 1e-5
        assert np.linalg.norm(dw - expected_dw_prev, ord="fro") < 1e-5

    def test_backward_propagation_with_relu_activation(self):
        np.random.seed(2)
        da = np.random.randn(1, 2)
        a_prev = np.random.randn(3, 2)
        w = np.random.randn(1, 3)
        z = np.array([[0.04153939, -1.11792545]])
        layer = FullyConnectedLayer(num_neurons=w.shape[0], activation_func=ReLUActivationFunction())
        layer.params["W"] = w
        # Update the current status of the cache
        layer.z_val = z
        layer.a_prev = a_prev
        da_prev = layer.propagate_backward(da_val=da)
        expected_da_prev = np.array([[0.44090989, -0.0], [0.37883606, -0.0], [-0.2298228, 0.0]])
        assert np.linalg.norm(da_prev - expected_da_prev, ord="fro") < 1e-5

    def test_backward_propagation_with_sigmoid_activation(self):
        np.random.seed(2)
        da = np.random.randn(1, 2)
        a_prev = np.random.randn(3, 2)
        w = np.random.randn(1, 3)
        z = np.array([[0.04153939, -1.11792545]])
        layer = FullyConnectedLayer(num_neurons=w.shape[0], activation_func=SigmoidActivationFunction())
        layer.params["W"] = w
        # Update the current status of the cache
        layer.z_val = z
        layer.a_prev = a_prev
        da_prev = layer.propagate_backward(da_val=da)
        expected_da_prev = np.array([[0.11017994, 0.01105339], [0.09466817, 0.00949723], [-0.05743092, -0.00576154]])
        assert np.linalg.norm(da_prev - expected_da_prev, ord="fro") < 1e-5

    def test_backward_propgation(self):
        np.random.seed(3)
        al = np.random.randn(1, 2)
        y = np.array([[1, 0]])

        a1 = np.random.randn(4, 2)
        w1 = np.random.randn(3, 4)
        b1 = np.random.randn(3, 1)
        z1 = np.random.randn(3, 2)

        a2 = np.random.randn(3, 2)
        w2 = np.random.randn(1, 3)
        b2 = np.random.randn(1, 1)
        z2 = np.random.randn(1, 2)

        net1 = DNN()
        net1.add_layer(InputLayer(num_neurons=4))
        net1.add_layer(FullyConnectedLayer(num_neurons=3, activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=1, activation_func=SigmoidActivationFunction()))

        # Assign all parameters of the network.
        net1.layers[1].params["W"] = w1
        net1.layers[1].params["b"] = b1
        net1.layers[2].params["W"] = w2
        net1.layers[2].params["b"] = b2

        # Write the hidden variables
        net1.layers[1].z_val = z1
        net1.layers[2].z_val = z2
        net1.layers[1].a_prev = a1
        net1.layers[2].a_prev = a2

        net1.propagate_backward(yhat_val=al, y_val=y)
        expected_dw1 = np.array(
            [
                [0.41010002, 0.07807203, 0.13798444, 0.10502167],
                [
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                ],
                [0.05283652, 0.01005865, 0.01777766, 0.0135308],
            ]
        )
        expected_db1 = np.array([[-0.22007063], [0.0], [-0.02835349]])
        assert np.linalg.norm(net1.layers[1].grads["dw"] - expected_dw1, ord="fro") < 1e-5
        assert np.linalg.norm(net1.layers[1].grads["db"] - expected_db1, ord="fro") < 1e-5
