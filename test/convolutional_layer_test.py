"""Unit tests for the convolutional layers."""
import numpy as np
import pytest

from dnn.core.layers import Convolutional


class TestConvolutionaLayer:
    def setup_class(self):
        self.layer = Convolutional(filter_size=(3, 3, 4, 8), stride=2, pad=1)

    def test_forward_propagation(self):
        """Tests forward propagation."""
        np.random.seed(1)
        a_prev = np.random.randn(10, 5, 7, 4)
        self.layer.params["W"] = np.random.randn(3, 3, 4, 8)
        self.layer.params["b"] = np.random.randn(1, 1, 1, 8)
        output = self.layer.propagate_forward(a_prev=a_prev)
        assert np.mean(output) == pytest.approx(0.692360880758, rel=1e-6)
        assert (
            np.linalg.norm(
                output[3, 2, 1]
                - np.array(
                    [-1.28912231, 2.27650251, 6.61941931, 0.95527176, 8.25132576, 2.31329639, 13.00689405, 2.34576051]
                )
            )
            < 1e-6
        )
