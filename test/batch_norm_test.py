"""Unit tests for batch normalization."""
import numpy as np

from dnn.core.layers.batch_norm import BatchNormLayer
from dnn.core.layers.layer import ForwardPropagationMode
from dnn.util import random_mini_batches


class TestBatchNorm:
    def test_forward_propagation_train_mode(self):
        """Checks the forward pass and makes sure it produces the right mean and deviation."""
        n_x = 3
        m = 200
        expected_mean = np.array([[1.0], [2.0], [3.0]])
        expected_std = np.array([[11.0], [12.0], [13.0]])

        layer = BatchNormLayer(num_neurons=n_x)
        layer.params["gamma"] = expected_std
        layer.params["beta"] = expected_mean
        z_val = np.random.randn(n_x, m)
        y_val = layer.propagate_forward(z_val, propagation_mode=ForwardPropagationMode.Train)
        assert np.linalg.norm(np.mean(y_val, axis=1, keepdims=True) - expected_mean) < 1e-5
        assert np.linalg.norm(np.std(y_val, axis=1, keepdims=True) - expected_std) < 1e-5

    def test_forward_propagation_predict_mode(self):
        """Test the forward propagation in the test mode."""
        n_x = 3
        m = 1000000  # Get enough samples so that we should have the mean of 0 and the variance of 1 coming out.
        x = np.random.randn(n_x, m)
        y = np.random.randn(1, m)
        mini_batches = random_mini_batches(x, y, mini_batch_size=200)
        layer = BatchNormLayer(num_neurons=n_x)
        expected_mean = np.array([[1.0], [2.0], [3.0]])
        expected_std = np.array([[11.0], [12.0], [13.0]])
        layer.params["gamma"] = expected_std
        layer.params["beta"] = expected_mean
        # Process all the mini-batches to update the mean and variance of the batches.
        for x_batch, _ in mini_batches:
            _ = layer.propagate_forward(x_batch, propagation_mode=ForwardPropagationMode.Train)

        # Generate test data
        test_x = np.random.randn(n_x, 50000)
        y_val = layer.propagate_forward(test_x, propagation_mode=ForwardPropagationMode.Predict)
        assert np.linalg.norm(layer.train_mu - np.mean(x, axis=1, keepdims=True)) < 1e-5
        assert np.linalg.norm(layer.train_var - np.ones((n_x, 1)) < 1e-5)
        # We expect this difference to be small but not too small because the mean and the variance is of training data.
        assert np.linalg.norm(np.mean(y_val, axis=1, keepdims=True) - expected_mean) < 1e-1
        assert np.linalg.norm(np.std(y_val, axis=1, keepdims=True) - expected_std) < 1e-1

    def test_backprop(self):
        """Tests the backward propagation by comparing it against the numerical gradients."""
        n_x = 4
        m = 100
        z_val = 5 * np.random.randn(n_x, m) + 12  # Input
        dy = np.random.randn(n_x, m)  # Derivative of the loss of the output of batch norm
        expected_std = np.ones((n_x, 1))
        expected_mean = np.random.rand(n_x, 1)

        layer = BatchNormLayer(num_neurons=n_x)
        layer.params["gamma"] = expected_std
        layer.params["beta"] = expected_mean

        # Forward propagate to update mu_batch, var_batch, x_hat
        layer.propagate_forward(z_val=z_val, propagation_mode=ForwardPropagationMode.Train)

        # Now, backward propagate to
        layer.propagate_backward(dy_val=dy)

        # TODO: implement the parameter update.
