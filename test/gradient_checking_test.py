from typing import Tuple

import numpy as np

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.activation import SoftmaxActivationFunction
from dnn.tools import EndToEndGradientChecker


class TestGradientChecker:
    @staticmethod
    def _generate_data_and_parameters_for_binary_classification() -> Tuple[np.ndarray]:
        np.random.seed(1)
        x = np.random.randn(4, 3)
        y = np.array([1, 1, 0]).reshape((1, 3))
        w1 = np.random.randn(5, 4)
        b1 = np.random.randn(5, 1)
        w2 = np.random.randn(3, 5)
        b2 = np.random.randn(3, 1)
        w3 = np.random.randn(1, 3)
        b3 = np.random.randn(1, 1)
        return x, y, w1, b1, w2, b2, w3, b3

    @staticmethod
    def _generate_data_and_parameters_for_multiclass_classification() -> Tuple[np.ndarray]:
        np.random.seed(1)
        x = np.random.randn(4, 3)
        y = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        w1 = np.random.randn(5, 4)
        b1 = np.random.randn(5, 1)
        w2 = np.random.randn(6, 5)
        b2 = np.random.randn(6, 1)
        w3 = np.random.randn(3, 6)
        b3 = np.random.randn(3, 1)
        return x, y, w1, b1, w2, b2, w3, b3

    def test_full_dnn_binary_classification_no_regularization(self):
        x, y, w1, b1, w2, b2, w3, b3 = self._generate_data_and_parameters_for_binary_classification()
        net1 = DNN(l2_lambd=0.0)
        net1.add_layer(InputLayer(num_neurons=x.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SigmoidActivationFunction()))
        net1.layers[1].params = {"W": w1, "b": b1}
        net1.layers[2].params = {"W": w2, "b": b2}
        net1.layers[3].params = {"W": w3, "b": b3}
        grad_checker = EndToEndGradientChecker(net=net1)
        grad_checker.check(x=x, y=y)

    def test_full_dnn_binary_classification_with_regularization(self):
        x, y, w1, b1, w2, b2, w3, b3 = self._generate_data_and_parameters_for_binary_classification()
        l2_lambd = 1.0
        net1 = DNN(l2_lambd=l2_lambd)
        net1.add_layer(InputLayer(num_neurons=x.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SigmoidActivationFunction()))
        net1.layers[1].params = {"W": w1, "b": b1}
        net1.layers[2].params = {"W": w2, "b": b2}
        net1.layers[3].params = {"W": w3, "b": b3}
        grad_checker = EndToEndGradientChecker(net=net1)
        grad_checker.check(x=x, y=y)

    def test_multiclass_classification_no_regularization(self):
        x, y, w1, b1, w2, b2, w3, b3 = self._generate_data_and_parameters_for_multiclass_classification()
        net1 = DNN(l2_lambd=0.0)
        net1.add_layer(InputLayer(num_neurons=x.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SoftmaxActivationFunction()))
        net1.layers[1].params = {"W": w1, "b": b1}
        net1.layers[2].params = {"W": w2, "b": b2}
        net1.layers[3].params = {"W": w3, "b": b3}
        grad_checker = EndToEndGradientChecker(net=net1)
        grad_checker.check(x=x, y=y)
