import numpy as np

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.layers.dropout import DropoutLayer


class TestDropOut:
    def test_forward_propagation_with_dropout(self):
        np.random.seed(1)
        w1 = np.array([[-1.09989127, -0.17242821, -0.87785842], [0.04221375, 0.58281521, -1.10061918]])
        w2 = np.array([[0.50249434, 0.90085595], [-0.68372786, -0.12289023], [-0.93576943, -0.26788808]])
        w3 = np.array([[-0.6871727, -0.84520564, -0.67124613]])
        b1 = np.array([[1.14472371], [0.90159072]])
        b2 = np.array([[0.53035547], [-0.69166075], [-0.39675353]])
        b3 = np.array([[-0.0126646]])
        x = np.array(
            [
                [1.62434536, -0.61175641, -0.52817175, -1.07296862, 0.86540763],
                [-2.3015387, 1.74481176, -0.7612069, 0.3190391, -0.24937038],
                [1.46210794, -2.06014071, -0.3224172, -0.38405435, 1.13376944],
            ]
        )
        net1 = DNN(l2_lambd=0.0)
        net1.add_layer(InputLayer(num_neurons=x.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(DropoutLayer(keep_prob=0.7))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(DropoutLayer(keep_prob=0.7))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SigmoidActivationFunction()))
        net1.layers[1].params = {"W": w1, "b": b1}
        net1.layers[3].params = {"W": w2, "b": b2}
        net1.layers[5].params = {"W": w3, "b": b3}
        assert (
            np.linalg.norm(
                net1.propagate_forward(x) - np.array([[0.36974721, 0.00305176, 0.04565099, 0.49683389, 0.36974721]])
            )
            < 1e-5
        )

    def test_backward_propagation_with_dropout(self):
        """Test the backward propagation with dropout."""
        x = np.array(
            [
                [1.62434536, -0.61175641, -0.52817175, -1.07296862, 0.86540763],
                [-2.3015387, 1.74481176, -0.7612069, 0.3190391, -0.24937038],
                [1.46210794, -2.06014071, -0.3224172, -0.38405435, 1.13376944],
            ]
        )
        y = np.array([[1, 1, 0, 1, 0]])
        z1 = np.array(
            [
                [-1.52855314, 3.32524635, 2.13994541, 2.60700654, -0.75942115],
                [-1.98043538, 4.1600994, 0.79051021, 1.46493512, -0.45506242],
            ]
        )
        d1 = np.array([[True, False, True, True, True], [True, True, True, True, False]], dtype=bool)
        a1 = np.array([[0.0, 0.0, 4.27989081, 5.21401307, 0.0], [0.0, 8.32019881, 1.58102041, 2.92987024, 0.0]])
        w1 = np.array([[-1.09989127, -0.17242821, -0.87785842], [0.04221375, 0.58281521, -1.10061918]])
        b1 = np.array([[1.14472371], [0.90159072]])
        z2 = np.array(
            [
                [0.53035547, 8.02565606, 4.10524802, 5.78975856, 0.53035547],
                [-0.69166075, -1.71413186, -3.81223329, -4.61667916, -0.69166075],
                [-0.39675353, -2.62563561, -4.82528105, -6.0607449, -0.39675353],
            ]
        )
        d2 = np.array(
            [[True, False, True, False, True], [False, True, False, True, True], [False, False, True, False, False]],
            dtype=bool,
        )
        a2 = np.array(
            [[1.06071093, 0.0, 8.21049603, 0.0, 1.06071093], [0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0]]
        )
        w2 = np.array([[0.50249434, 0.90085595], [-0.68372786, -0.12289023], [-0.93576943, -0.26788808]])
        b2 = np.array([[0.53035547], [-0.69166075], [-0.39675353]])
        z3 = np.array([[-0.7415562, -0.0126646, -5.65469333, -0.0126646, -0.7415562]])
        a3 = np.array([[0.32266394, 0.49683389, 0.00348883, 0.49683389, 0.32266394]])
        w3 = np.array([[-0.6871727, -0.84520564, -0.67124613]])
        b3 = np.array([[-0.0126646]])
        net1 = DNN(l2_lambd=0)
        net1.add_layer(InputLayer(num_neurons=x.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(DropoutLayer(keep_prob=0.8))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(DropoutLayer(keep_prob=0.8))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SigmoidActivationFunction()))
        net1.layers[1].params = {"W": w1, "b": b1}
        net1.layers[3].params = {"W": w2, "b": b2}
        net1.layers[5].params = {"W": w3, "b": b3}

        # Write latent variaables
        net1.layers[1].z_val = z1
        net1.layers[3].z_val = z2
        net1.layers[5].z_val = z3

        # Assign the drop put masl
        net1.layers[2].dropout_mask = d1
        net1.layers[4].dropout_mask = d2

        # Assign the cached activation output
        net1.layers[1].a_prev = x
        net1.layers[3].a_prev = a1
        net1.layers[5].a_prev = a2

        net1.propagate_backward(yhat_val=a3, y_val=y)
        assert (
            np.linalg.norm(
                net1.layers[1].grads["dw"]
                - np.array([[0.00019884, 0.00028657, 0.00012138], [0.00035647, 0.00051375, 0.00021761]])
            )
            < 1e-5
        )

        assert np.linalg.norm(net1.layers[1].grads["db"] - np.array([[-0.00037647], [-0.00067492]])) < 1e-5

        assert (
            np.linalg.norm(net1.layers[3].grads["dw"] - np.array([[-0.00256518, -0.0009476], [0.0, 0.0], [0.0, 0.0]]))
            < 1e-5
        )

        assert np.linalg.norm(net1.layers[3].grads["db"] - np.array([[0.06033089], [0.0], [0.0]])) < 1e-5

        assert np.linalg.norm(net1.layers[5].grads["dw"] - np.array([[-0.06951191, 0.0, 0.0]])) < 1e-5

        assert np.linalg.norm(net1.layers[5].grads["db"] - np.array([[-0.2715031]])) < 1e-5
