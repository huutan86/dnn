import numpy as np
import pytest

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.loss import compute_total_loss


class TestRegularization:
    def test_loss_compute_with_regularization(self):
        """Tests the computing of the loss with regularization."""
        y3 = np.array([[1, 1, 0, 1, 0]])
        a3 = np.array([[0.40682402, 0.01629284, 0.16722898, 0.10118111, 0.40682402]])

        w1 = np.array([[1.62434536, -0.61175641, -0.52817175], [-1.07296862, 0.86540763, -2.3015387]])
        w2 = np.array([[0.3190391, -0.24937038], [1.46210794, -2.06014071], [-0.3224172, -0.38405435]])
        w3 = np.array([[-0.87785842, 0.04221375, 0.58281521]])

        net1 = DNN(l2_lambd=0.1)
        net1.add_layer(InputLayer(num_neurons=211))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SigmoidActivationFunction()))
        net1.layers[1].params["W"] = w1
        net1.layers[2].params["W"] = w2
        net1.layers[3].params["W"] = w3
        reg_loss, non_reg_loss = compute_total_loss(
            a3, y3, l2_lambd=net1.l2_lambda, network_params=net1.parameters, loss_type=net1.loss_type
        )
        m = y3.shape[1]
        total_loss = (reg_loss + non_reg_loss) / m
        assert total_loss == pytest.approx(1.78648594516, rel=1e-5)

    def test_regularization_parameter_update(self):
        """Tests the update of W with the regularization."""
        x = np.array(
            [
                [1.62434536, -0.61175641, -0.52817175, -1.07296862, 0.86540763],
                [-2.3015387, 1.74481176, -0.7612069, 0.3190391, -0.24937038],
                [1.46210794, -2.06014071, -0.3224172, -0.38405435, 1.13376944],
            ]
        )
        y = np.array([[1, 1, 0, 1, 0]])
        z1 = np.array(
            [
                [-1.52855314, 3.32524635, 2.13994541, 2.60700654, -0.75942115],
                [-1.98043538, 4.1600994, 0.79051021, 1.46493512, -0.45506242],
            ]
        )
        a1 = np.array([[0.0, 3.32524635, 2.13994541, 2.60700654, 0.0], [0.0, 4.1600994, 0.79051021, 1.46493512, 0.0]])
        w1 = np.array([[-1.09989127, -0.17242821, -0.87785842], [0.04221375, 0.58281521, -1.10061918]])
        b1 = np.array([[1.14472371], [0.90159072]])
        z2 = np.array(
            [
                [0.53035547, 5.94892323, 2.31780174, 3.16005701, 0.53035547],
                [-0.69166075, -3.47645987, -2.25194702, -2.65416996, -0.69166075],
                [-0.39675353, -4.62285846, -2.61101729, -3.22874921, -0.39675353],
            ]
        )
        a2 = np.array(
            [
                [0.53035547, 5.94892323, 2.31780174, 3.16005701, 0.53035547],
                [0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0],
            ]
        )
        w2 = np.array([[0.50249434, 0.90085595], [-0.68372786, -0.12289023], [-0.93576943, -0.26788808]])
        b2 = np.array([[0.53035547], [-0.69166075], [-0.39675353]])
        z3 = np.array([[-0.3771104, -4.10060224, -1.60539468, -2.18416951, -0.3771104]])
        a3 = np.array([[0.40682402, 0.01629284, 0.16722898, 0.10118111, 0.40682402]])
        w3 = np.array([[-0.6871727, -0.84520564, -0.67124613]])
        b3 = np.array([[-0.0126646]])
        net1 = DNN(l2_lambd=0.7)
        net1.add_layer(InputLayer(num_neurons=x.shape[0]))
        net1.add_layer(FullyConnectedLayer(num_neurons=w1.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w2.shape[0], activation_func=ReLUActivationFunction()))
        net1.add_layer(FullyConnectedLayer(num_neurons=w3.shape[0], activation_func=SigmoidActivationFunction()))

        # Override the parameters.
        net1.layers[1].params["W"] = w1
        net1.layers[2].params["W"] = w2
        net1.layers[3].params["W"] = w3

        net1.layers[1].params["b"] = b1
        net1.layers[2].params["b"] = b2
        net1.layers[3].params["b"] = b3

        # Overwrite the hidden variables.
        net1.layers[1].z_val = z1
        net1.layers[2].z_val = z2
        net1.layers[3].z_val = z3

        # Overwrite the previous activation values for previous layers
        net1.layers[1].a_prev = x
        net1.layers[2].a_prev = a1
        net1.layers[3].a_prev = a2

        # Propagate backward
        net1.propagate_backward(yhat_val=a3, y_val=y)
        assert (
            np.linalg.norm(
                net1.layers[1].grads["dw"]
                - np.array([[-0.25604646, 0.12298827, -0.28297129], [-0.17706303, 0.34536094, -0.4410571]])
            )
            < 1e-5
        )
        assert (
            np.linalg.norm(
                net1.layers[2].grads["dw"]
                - np.array([[0.79276486, 0.85133918], [-0.0957219, -0.01720463], [-0.13100772, -0.03750433]])
            )
            < 1e-5
        )
        assert np.linalg.norm(net1.layers[3].grads["dw"] - np.array([[-1.77691347, -0.11832879, -0.09397446]])) < 1e-5
