"""Provides unit test for different optimizers."""
import numpy as np

from dnn import DNN
from dnn.core import FullyConnectedLayer
from dnn.core import InputLayer
from dnn.core.activation import ReLUActivationFunction
from dnn.core.activation import SigmoidActivationFunction
from dnn.core.optimization import optimizer
from dnn.core.optimization import OptimizerType


class TestUpdateParameters:
    @staticmethod
    def _initialize_test_case(optimizer_type: OptimizerType) -> DNN:
        net1 = DNN()
        net1.add_layer(InputLayer(num_neurons=3))
        net1.add_layer(
            FullyConnectedLayer(
                num_neurons=2,
                activation_func=ReLUActivationFunction(),
                optimizer=optimizer(optimizer_type),
            )
        )
        net1.add_layer(
            FullyConnectedLayer(
                num_neurons=3,
                activation_func=SigmoidActivationFunction(),
                optimizer=optimizer(optimizer_type),
            )
        )

        # Set all the parameters
        np.random.seed(1)
        net1.layers[1].parameters["W"] = np.random.randn(2, 3)
        net1.layers[1].parameters["b"] = np.random.randn(2, 1)
        net1.layers[2].parameters["W"] = np.random.randn(3, 3)
        net1.layers[2].parameters["b"] = np.random.randn(3, 1)

        net1.layers[1].grads["dw"] = np.random.randn(2, 3)
        net1.layers[1].grads["db"] = np.random.randn(2, 1)
        net1.layers[2].grads["dw"] = np.random.randn(3, 3)
        net1.layers[2].grads["db"] = np.random.randn(3, 1)
        return net1

    def test_update_parameters_with_gd(self):
        """Tests update parameters with gradient descent."""
        net1 = self._initialize_test_case(optimizer_type=OptimizerType.GradientDescent)
        net1.update_parameters(learning_rate=0.01)
        assert (
            np.linalg.norm(
                net1.layers[1].parameters["W"]
                - np.array([[1.63535156, -0.62320365, -0.53718766], [-1.07799357, 0.85639907, -2.29470142]])
            )
            < 1e-5
        )

        assert np.linalg.norm(net1.layers[1].parameters["b"] - np.array([[1.74604067], [-0.75184921]])) < 1e-5
        assert (
            np.linalg.norm(
                net1.layers[2].parameters["W"]
                - np.array(
                    [
                        [0.32171798, -0.25467393, 1.46902454],
                        [-2.05617317, -0.31554548, -0.3756023],
                        [1.1404819, -1.09976462, -0.1612551],
                    ]
                )
            )
            < 1e-5
        )
        assert (
            np.linalg.norm(net1.layers[2].parameters["b"] - np.array([[-0.88020257], [0.02561572], [0.57539477]]))
            < 1e-5
        )

    def test_update_parameters_with_momentum(self):
        """Tests update parameters with momentum."""
        net1 = self._initialize_test_case(optimizer_type=OptimizerType.Momentum)
        net1.update_parameters(learning_rate=0.01)
        assert (
            np.linalg.norm(
                net1.layers[1].parameters["W"]
                - np.array([[1.62544598, -0.61290114, -0.52907334], [-1.07347112, 0.86450677, -2.30085497]])
            )
            < 1e-5
        )

        assert np.linalg.norm(net1.layers[1].parameters["b"] - np.array([[1.74493465], [-0.76027113]])) < 1e-5
        assert (
            np.linalg.norm(
                net1.layers[2].parameters["W"]
                - np.array(
                    [
                        [0.31930698, -0.24990073, 1.4627996],
                        [-2.05974396, -0.32173003, -0.38320915],
                        [1.13444069, -1.0998786, -0.1713109],
                    ]
                )
            )
            < 1e-5
        )
        assert (
            np.linalg.norm(net1.layers[2].parameters["b"] - np.array([[-0.87809283], [0.04055394], [0.58207317]]))
            < 1e-5
        )

        assert (
            np.linalg.norm(
                net1.layers[1].optimizer.update["dw"]
                - np.array([[-0.11006192, 0.11447237, 0.09015907], [0.05024943, 0.09008559, -0.06837279]])
            )
            < 1e-5
        )

        assert np.linalg.norm(net1.layers[1].optimizer.update["db"] - np.array([[-0.01228902], [-0.09357694]])) < 1e-5
        assert (
            np.linalg.norm(
                net1.layers[2].optimizer.update["dw"]
                - np.array(
                    [
                        [-0.02678881, 0.05303555, -0.06916608],
                        [-0.03967535, -0.06871727, -0.08452056],
                        [-0.06712461, -0.00126646, -0.11173103],
                    ]
                )
            )
            < 1e-5
        )
        assert (
            np.linalg.norm(net1.layers[2].optimizer.update["db"] - np.array([[0.02344157], [0.16598022], [0.07420442]]))
            < 1e-5
        )

    def test_update_parameters_with_adam(self):
        net1 = self._initialize_test_case(optimizer_type=OptimizerType.Adam)

        # Overwrite the number of step index t.
        net1.layers[1].optimizer.t = 2
        net1.layers[2].optimizer.t = 2
        net1.update_parameters(learning_rate=0.01)
        assert (
            np.linalg.norm(
                net1.layers[1].parameters["W"]
                - np.array([[1.63178673, -0.61919778, -0.53561312], [-1.08040999, 0.85796626, -2.29409733]])
            )
            < 1e-5
        )

        assert np.linalg.norm(net1.layers[1].parameters["b"] - np.array([[1.75225313], [-0.75376553]])) < 1e-5
        assert (
            np.linalg.norm(
                net1.layers[2].parameters["W"]
                - np.array(
                    [
                        [0.32648046, -0.25681174, 1.46954931],
                        [-2.05269934, -0.31497584, -0.37661299],
                        [1.1412108, -1.09244991, -0.16498684],
                    ]
                )
            )
            < 1e-5
        )
        assert (
            np.linalg.norm(net1.layers[2].parameters["b"] - np.array([[-0.88529979], [0.03477238], [0.57537385]]))
            < 1e-5
        )
